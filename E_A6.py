# Autor_ Angel Bolivar Contento Guaman
# Email_ angel.b.contento@unl.edu.ec
#Istruccions__Given a list of countries and cities of each country, then given the names of the cities.
# For each city print the country in which it is located.

# Read a string:
# s = input()
# Print a value:
# print(s)

num = int(input())
Location ={}
for i in range (num):
    a = list(input().split())
    Location[a[0]] = a[1:]
m = int(input())
for t in range (m):
    b = input()
    for country, city in Location.items():
        if b in city:
            print(country)