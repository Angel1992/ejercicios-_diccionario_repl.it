# Autor_ Angel Bolivar Contento Guaman
# Email_ angel.b.contento@unl.edu.ec
# Instruccions__ Given the text: the first line contains the number of lines, then given
# the lines of words. Print the word in the text that occurs most often.
#If there are many such words, print the one that is less in the alphabetical order.

# Read a string:
# s = input()
# Print a value:
# print(s)

cont_palabra = {}
for i in range(int(input())):
  palabras = input().split()
  for palabra in palabras:
    if palabra not in cont_palabra:
      cont_palabra[palabra] = 0
    cont_palabra[palabra] += 1
max_frecuencia = max(cont_palabra.values())
for palabra in sorted(cont_palabra):
  if cont_palabra[palabra] == max_frecuencia:
    print(palabra)
    break
