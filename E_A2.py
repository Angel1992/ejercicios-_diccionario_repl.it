# Autor_ Angel Bolivar Contento Guaman
# Email_ angel.b.contento@unl.edu.ec
# Instruccions_ You are given a dictionary consisting of word pairs.
# Every word is a synonym of the other word in its pair.
# All the words in the dictionary are different.

# Read a string:
# s = input()
# Print a value:
# print(s)

prmt = int(input())
dictionary = {}
s = list(input().split())
for i in range (prmt):
    dictionary[s[0]] = s[1]
    if i < prmt-1:
        s = list(input().split())
tex = input()
for k, v in dictionary.items():
    if k == tex:
        print(v)
    if v == tex:
        print(k)