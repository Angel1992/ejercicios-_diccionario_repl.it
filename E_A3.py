# Autor_ Angel Bolivar Contento Guaman
# Email_ angel.b.contento@unl.edu.ec
# Instruccions__The first line contains the number of records. After that,
# each entry contains the name of the candidate and the number of votes they
# got in some state. Count the results of the elections:
# sum the number of votes for each candidate. Print candidates in the alphabetical order.

# Read a string:
# s = input()
# Print a value:
# print(s)
n = int(input())
votos_total = {}
for i in range(n):
  candidato, num_votos = input().split()
  if candidato not in votos_total:
    votos_total[candidato] = 0
  votos_total[candidato] += int(num_votos)
for candidato in sorted(votos_total):
  print(candidato, votos_total[candidato])