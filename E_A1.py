# Autor_ Angel Bolivar Contento Guaman
# Email_ angel.b.contento@unl.edu.ec
#Instruccions__The text is given in a single line. For each word of the text count the number
# of its occurrences before it.

# Read a string:
# s = input()
# Print a value:
# print(s)
tex_s = input().split()
frec = {}
for palabra in tex_s:
  if palabra not in frec:
    frec[palabra] = 0
  print(frec[palabra], end=' ')
  frec[palabra] += 1
